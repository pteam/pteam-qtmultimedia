load(qt_build_config)

#DEFINES += QT_DEBUG_AVF
# Avoid clash with a variable named `slots' in a Quartz header
CONFIG += no_keywords

TARGET = qavfmediaplayer
QT += multimedia-private network

PLUGIN_TYPE = mediaservice
PLUGIN_CLASS_NAME = AVFMediaPlayerServicePlugin
load(qt_plugin)

LIBS += -framework AVFoundation -framework CoreMedia -framework QuartzCore

DEFINES += QMEDIA_AVF_MEDIAPLAYER

HEADERS += \
    avfmediaplayercontrol.h \
    avfmediaplayermetadatacontrol.h \
    avfmediaplayerservice.h \
    avfmediaplayerserviceplugin.h \
    avfvideooutput.h \
    avfvideowindowcontrol.h \
    avfmediaplayersession.h \
    avfvideoassetoutput.h \
    avfvideosampleprovider_ios.h

SOURCES += \
    avfmediaplayersession.cpp

OBJECTIVE_SOURCES += \
    avfmediaplayercontrol.mm \
    avfmediaplayermetadatacontrol.mm \
    avfmediaplayerservice.mm \
    avfmediaplayerserviceplugin.mm \
    avfvideooutput.mm \
    avfvideowindowcontrol.mm \
    avfmediaplayersession_mac.mm \
    avfvideoassetoutput.mm \
    avfvideosampleprovider_ios.mm

    qtHaveModule(widgets) {
        QT += multimediawidgets-private
        HEADERS += \
            avfvideowidgetcontrol.h \
            avfvideowidget.h

        OBJECTIVE_SOURCES += \
            avfvideowidgetcontrol.mm \
            avfvideowidget.mm
    }

!ios {
    LIBS += -framework AppKit

    HEADERS += \
        avfmediaplayersession_mac.h \
        avfvideorenderercontrol.h \
        avfdisplaylink.h
    OBJECTIVE_SOURCES += \
        avfvideorenderercontrol.mm \
        avfdisplaylink.mm

    contains(QT_CONFIG, opengl.*) {
        HEADERS += \
            avfvideoframerenderer.h
        OBJECTIVE_SOURCES += \
            avfvideoframerenderer.mm
    }
} else {
    HEADERS += \
        avfscreenlink.h \
        avfvideorenderercontrol_ios.h \
        avfmediaplayersession_ios.h
    OBJECTIVE_SOURCES += \
        avfscreenlink.mm \
        avfvideorenderercontrol_ios.mm \
        avfmediaplayersession_ios.mm
}

OTHER_FILES += \
    avfmediaplayer.json
