/****************************************************************************
**
** Copyright (C) 2014 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL21$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia. For licensing terms and
** conditions see http://qt.digia.com/licensing. For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file. Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights. These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef AVFVIDEORENDERERCONTROL_IOS_H
#define AVFVIDEORENDERERCONTROL_IOS_H

#include <QtMultimedia/QVideoRendererControl>
#include <QtCore/QMutex>
#include <QtCore/QSize>

#include "avfvideoassetoutput.h"

QT_BEGIN_NAMESPACE

class AVFScreenLink;

class AVFVideoRendererControl_iOS : public QVideoRendererControl, public AVFVideoAssetOutput
{
    Q_OBJECT
    Q_INTERFACES(AVFVideoAssetOutput)
public:
    explicit AVFVideoRendererControl_iOS(QObject *parent = 0);
    virtual ~AVFVideoRendererControl_iOS();

    QAbstractVideoSurface *surface() const;
    void setSurface(QAbstractVideoSurface *surface);

    void setSampleProvider(AVFVideoSampleProvider_iOS* provider);
    void present(const QImage& image);

private Q_SLOTS:
    void renderSample(qint64 timestamp);

Q_SIGNALS:
    void surfaceChanged(QAbstractVideoSurface *surface);

private:
    QMutex m_mutex;
    QAbstractVideoSurface* m_surface;
    AVFVideoSampleProvider_iOS* m_provider;
    AVFScreenLink* m_link;
    qint64 start;

};

QT_END_NAMESPACE

#endif // AVFVIDEORENDERERCONTROL_IOS_H
