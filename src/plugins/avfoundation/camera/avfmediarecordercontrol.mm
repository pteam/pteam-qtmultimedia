/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "avfcameradebug.h"
#include "avfmediarecordercontrol.h"
#include "avfcamerasession.h"
#include "avfcameraservice.h"
#include "avfcameracontrol.h"

#include <UIKit/UIKit.h>
#include <Accelerate/accelerate.h>

#include <QtCore/QDebug>
#include <QtCore/qurl.h>
#include <QtCore/qfileinfo.h>
#include <QtMultimedia/qcameracontrol.h>


QT_USE_NAMESPACE

AVFMediaRecorderControl::AVFMediaRecorderControl(AVFCameraService *service, QObject *parent)
   : QMediaRecorderControl(parent)
   , recording(false)
   , initialized(false)
   , m_cameraControl(service->cameraControl())
   , m_session(service->session())
   , m_connected(true)
   , m_state(QMediaRecorder::StoppedState)
   , m_lastStatus(QMediaRecorder::UnloadedStatus)
   , m_recordingStarted(false)
   , m_recordingFinished(false)
   , m_muted(false)
   , m_volume(1.0)
{

    connect(m_cameraControl, SIGNAL(stateChanged(QCamera::State)), SLOT(updateStatus()));
    connect(m_cameraControl, SIGNAL(statusChanged(QCamera::Status)), SLOT(updateStatus()));
}

AVFMediaRecorderControl::~AVFMediaRecorderControl()
{

}

QUrl AVFMediaRecorderControl::outputLocation() const
{
    return m_outputLocation;
}

bool AVFMediaRecorderControl::setOutputLocation(const QUrl &location)
{
    m_outputLocation = location;
    return location.scheme() == QLatin1String("file") || location.scheme().isEmpty();
}

QMediaRecorder::State AVFMediaRecorderControl::state() const
{
    return m_state;
}

QMediaRecorder::Status AVFMediaRecorderControl::status() const
{
    QMediaRecorder::Status status = m_lastStatus;
    //bool videoEnabled = m_cameraControl->captureMode().testFlag(QCamera::CaptureVideo);

    if (m_cameraControl->status() == QCamera::ActiveStatus && m_connected) {
        if (m_state == QMediaRecorder::StoppedState) {
            if (m_recordingStarted && !m_recordingFinished)
                status = QMediaRecorder::FinalizingStatus;
            else
                status = QMediaRecorder::LoadedStatus;
        } else {
            status = m_recordingStarted ? QMediaRecorder::RecordingStatus :
                                            QMediaRecorder::StartingStatus;
        }
    } else {
        //camera not started yet
        status = m_cameraControl->state() == QCamera::ActiveState && m_connected ?
                    QMediaRecorder::LoadingStatus:
                    QMediaRecorder::UnloadedStatus;
    }

    return status;
}

void AVFMediaRecorderControl::updateStatus()
{
    QMediaRecorder::Status newStatus = status();

    if (m_lastStatus != newStatus) {
        qDebugCamera() << "Camera recorder status changed: " << m_lastStatus << " -> " << newStatus;
        m_lastStatus = newStatus;
        Q_EMIT statusChanged(m_lastStatus);
    }
}


qint64 AVFMediaRecorderControl::duration() const
{
    return qint64(recording ? durationTimer.elapsed() : 0);
}

bool AVFMediaRecorderControl::isMuted() const
{
    return m_muted;
}

qreal AVFMediaRecorderControl::volume() const
{
    return m_volume;
}

void AVFMediaRecorderControl::applySettings()
{
}

void AVFMediaRecorderControl::setState(QMediaRecorder::State state)
{
    if (m_state == state)
        return;

    qDebugCamera() << Q_FUNC_INFO << m_state << " -> " << state;

    switch (state) {
    case QMediaRecorder::RecordingState:
    {
        if (m_connected) {
            m_state = QMediaRecorder::RecordingState;
            m_recordingStarted = false;
            m_recordingFinished = false;

            QString outputLocationPath = m_outputLocation.scheme() == QLatin1String("file") ?
                        m_outputLocation.path() : m_outputLocation.toString();

            QUrl actualLocation = QUrl::fromLocalFile(
                        m_storageLocation.generateFileName(outputLocationPath,
                                                           QCamera::CaptureVideo,
                                                           QLatin1String("clip_"),
                                                           QLatin1String("mp4")));

            qDebugCamera() << "Video capture location:" << actualLocation.toString();

            NSError *error = nil;
            assetWriter = [[AVAssetWriter alloc]
                                            initWithURL:actualLocation.toNSURL()
                                            fileType:AVFileTypeMPEG4
                                            error:&error];

            initialized = false;
            durationTimer.start();
            recording = true;
            durationChanged(0);

            Q_EMIT actualLocationChanged(actualLocation);
        } else {
            Q_EMIT error(QMediaRecorder::FormatError, tr("Recorder not configured"));
        }

    } break;
    case QMediaRecorder::PausedState:
    {
        Q_EMIT error(QMediaRecorder::FormatError, tr("Recording pause not supported"));
        return;
    } break;
    case QMediaRecorder::StoppedState:
    {
        recording = false;
        initialized = false;
        [assetWriterInput markAsFinished];
        [assetWriter finishWritingWithCompletionHandler:^{
            [assetWriterInput release];
            [assetWriter release];
        }];

        m_state = QMediaRecorder::StoppedState;
        durationTimer = QTime();
    }
    }

    updateStatus();
    Q_EMIT stateChanged(m_state);
}

void AVFMediaRecorderControl::setMuted(bool muted)
{
    if (m_muted != muted) {
        m_muted = muted;
        Q_EMIT mutedChanged(muted);
    }
}

void AVFMediaRecorderControl::setVolume(qreal volume)
{
    if (m_volume != volume) {
        m_volume = volume;
        Q_EMIT volumeChanged(volume);
    }
}

void AVFMediaRecorderControl::handleRecordingStarted()
{
    m_recordingStarted = true;
    updateStatus();
}

void AVFMediaRecorderControl::handleRecordingFinished()
{
    m_recordingFinished = true;
    updateStatus();
}

void AVFMediaRecorderControl::handleRecordingFailed(const QString &message)
{
    m_recordingFinished = true;
    if (m_state != QMediaRecorder::StoppedState) {
        m_state = QMediaRecorder::StoppedState;
        Q_EMIT stateChanged(m_state);
    }
    updateStatus();

    Q_EMIT error(QMediaRecorder::ResourceError, message);
}

void AVFMediaRecorderControl::writeFrame(CMSampleBufferRef frame)
{
    if (recording && frame)
    {
        CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(frame);

        bool front = [m_session->videoCaptureDevice() position] == AVCaptureDevicePositionFront;

        uint8_t rotation = 0;
        switch ([UIApplication sharedApplication].keyWindow.rootViewController.interfaceOrientation) {
        case UIInterfaceOrientationPortrait:
            if (front)
            {
                rotation = 1;
            }
            else
            {
                rotation = 3;
            }
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            if (front)
            {
                rotation = 3;
            }
            else
            {
                rotation = 1;
            }
            break;
        case UIInterfaceOrientationLandscapeRight:
            if (front)
            {
                rotation = 2;
            }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            if (!front)
            {
                rotation = 2;
            }
            break;
        default:

            break;
        }

        if (!initialized)
        {
            int width = CVPixelBufferGetWidth(imageBuffer);
            int height = CVPixelBufferGetHeight(imageBuffer);
            if (rotation % 2 == 1)
            {
                qSwap(width, height);
            }

            qDebug() << "DIMENSIONS: " << width << height;

            NSDictionary *outputSettings =
                [NSDictionary dictionaryWithObjectsAndKeys:

                        [NSNumber numberWithInt:width], AVVideoWidthKey,
                        [NSNumber numberWithInt:height], AVVideoHeightKey,
                        AVVideoCodecH264, AVVideoCodecKey,

                        nil];

            assetWriterInput = [AVAssetWriterInput
                                               assetWriterInputWithMediaType:AVMediaTypeVideo
                                                              outputSettings:outputSettings];

            /* I'm going to push pixel buffers to it, so will need a
               AVAssetWriterPixelBufferAdaptor, to expect the same 32BGRA input as I've
               asked the AVCaptureVideDataOutput to supply */
            pixelBufferAdaptor =
                       [[AVAssetWriterInputPixelBufferAdaptor alloc]
                            initWithAssetWriterInput:assetWriterInput
                            sourcePixelBufferAttributes:
                                 [NSDictionary dictionaryWithObjectsAndKeys:
                                      [NSNumber numberWithInt:kCVPixelFormatType_32BGRA],
                                       kCVPixelBufferPixelFormatTypeKey,
                                 nil]];

            [assetWriter addInput:assetWriterInput];

            /* we need to warn the input to expect real time data incoming, so that it tries
               to avoid being unavailable at inopportune moments */
            assetWriterInput.expectsMediaDataInRealTime = YES;

            [assetWriter startWriting];
            [assetWriter startSessionAtSourceTime:kCMTimeZero];
            initialized = true;
        }

        if (rotation > 0)
        {

            CVPixelBufferLockBaseAddress(imageBuffer, 0);
            size_t width = CVPixelBufferGetWidth(imageBuffer);
            size_t height = CVPixelBufferGetHeight(imageBuffer);
            size_t newWidth = rotation % 2 == 0 ? width : height;
            size_t newHeight = rotation % 2 == 0 ? height : width;

            size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
            size_t currSize = bytesPerRow*height*sizeof(unsigned char);

            size_t bytesPerRowOut = 4*newWidth*sizeof(unsigned char);


            void *srcBuff = CVPixelBufferGetBaseAddress(imageBuffer);


            unsigned char *outBuff = (unsigned char*)malloc(currSize);

            vImage_Buffer ibuff = { srcBuff, height, width, bytesPerRow};
            vImage_Buffer ubuff = { outBuff, newHeight, newWidth, bytesPerRowOut};

            Pixel_8888 temp = {0, 0, 0, 0};
            vImage_Error err= vImageRotate90_ARGB8888 (&ibuff, &ubuff, rotation, temp, 0);
            if (err != kvImageNoError) NSLog(@"%ld", err);

            CVPixelBufferRef buffer = NULL;
            int res = CVPixelBufferCreateWithBytes(NULL, newWidth, newHeight, kCVPixelFormatType_32BGRA, outBuff, bytesPerRowOut, NULL, 0, NULL, &buffer);
            if(assetWriterInput.readyForMoreMediaData)
                [pixelBufferAdaptor appendPixelBuffer:buffer
                                withPresentationTime:CMTimeMake(durationTimer.elapsed(), 1000)];
            free(outBuff);
        }
        else
        {
            if(assetWriterInput.readyForMoreMediaData)
                [pixelBufferAdaptor appendPixelBuffer:imageBuffer
                                 withPresentationTime:CMTimeMake(durationTimer.elapsed(), 1000)];
        }
    }
}

#include "moc_avfmediarecordercontrol.cpp"
